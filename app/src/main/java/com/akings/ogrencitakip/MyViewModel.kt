package com.akings.ogrencitakip

import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.akings.ogrencitakip.model.Book
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.tasks.await
import java.util.*
import javax.inject.Inject

@HiltViewModel
class MyViewModel @Inject constructor(savedStateHandle: SavedStateHandle) : ViewModel() {

    val db = FirebaseFirestore.getInstance()

    val state = mutableStateOf(TextFieldState())

    init {
        savedStateHandle.get<String>("bookId")?.let { bookId ->
            getBook(bookId)
        }
    }

    fun addNewBook(kitap: String, ders: String) {
        val book = Book(
            id = UUID.randomUUID().toString(),
            kitap = kitap,
            ders = ders
        )
        addBook(book)
    }

    fun addBook(book: Book) {
        try {
            val veri = db.collection("books").document(book.id)
            veri.set(book)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun updateBook(newKitap: String, newDers: String) {
        if (state.value.book == null) {
            state.value = TextFieldState(error = "Book is null")
            return
        }
        val bookEdited = state.value.book!!.copy(kitap = newKitap, ders = newDers)
        updateBook1(bookEdited.id, bookEdited)
    }

    fun updateBook1(bookId: String, book: Book) {
        try {
            val map = mapOf(
                "kitap" to book.kitap,
                "ders" to book.ders
            )

            db.collection("books").document(bookId).update(map)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getBook(bookId: String) {
        println("MyViewModel getBook çağrıldı")
        getBookId(bookId).onEach { result ->
            when (result) {
                is Result.Error -> {
                    state.value = TextFieldState(error = result.message ?: "Unexpected error")
                }
                is Result.Loading -> {
                    state.value = TextFieldState(isLoading = true)
                }
                is Result.Success -> {
                    state.value = TextFieldState(book = result.data)
                }
            }
        }.launchIn(viewModelScope)
    }

    fun getBookId(bookId: String): Flow<Result<Book>> = flow {
        println("getBookId çağrıldı")
        try {
            emit(Result.Loading<Book>())
            val book = db.collection("books")
                .whereGreaterThanOrEqualTo("id", bookId)
                .get()
                .await()
                .toObjects(Book::class.java)
                .first()

            emit(Result.Success<Book>(data = book))
            println("getBookId:$book")

        } catch (e: Exception) {
            emit(Result.Error<Book>(message = e.localizedMessage ?: "Error Desconocido"))
        }
    }

    fun getBooks(books: SnapshotStateList<Book>) {
        println("getBooks çağrıldı")
        db.collection("books").get().addOnSuccessListener {
            books.updateList(it.toObjects(Book::class.java))
        }.addOnFailureListener {
            books.updateList(listOf())
        }
    }

    fun deleteBook(book: Book, books: SnapshotStateList<Book>) {
        try {
            //books.removeAt(books.indexOf(book))
            books.remove(book)
            db.collection("books").document(book.id).delete()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}

data class TextFieldState(
    val isLoading: Boolean = false,
    val book: Book? = null,
    val error: String = ""
)