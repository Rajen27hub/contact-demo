package com.akings.ogrencitakip.model

data class Book(
    val id:String="",
    val kitap:String="",
    val ders: String ="",
)
