package com.akings.ogrencitakip.navigation

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.akings.ogrencitakip.ContactDetailScreen
import com.akings.ogrencitakip.ContactListScreen
import com.akings.ogrencitakip.MyViewModel

@Composable
fun Navigator() {

    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = Screen.ContactListScreen) {

        composable(route = Screen.ContactListScreen){
            val updateViewModel: MyViewModel = hiltViewModel()
            ContactListScreen(
                updateViewModel,
                onItemClick = {bookId ->navController.navigate(Screen.ContactDetailScreen + "?bookId=$bookId")},
                navigateToBookTextField = {navController.navigate(Screen.ContactDetailScreen)}
            )
        }

        composable(route = Screen.ContactDetailScreen + "?bookId={bookId}"){
            val updateViewModel: MyViewModel = hiltViewModel()
            val state = updateViewModel.state.value
            ContactDetailScreen(
                navController,
                state=state,
                updateBook = updateViewModel::updateBook,
                addNewBook = updateViewModel::addNewBook
            )
        }
    }
}

object Screen {
    const val ContactListScreen = "ContactListScreen"
    const val ContactDetailScreen = "ContactDetailScreen"
}