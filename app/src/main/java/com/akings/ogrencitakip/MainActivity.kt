package com.akings.ogrencitakip

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import com.akings.ogrencitakip.navigation.Navigator
import com.akings.ogrencitakip.ui.theme.OgrenciTakipTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            OgrenciTakipTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                ) {
                    Navigator()
                }
            }
        }
    }
}
